import numpy as np
import calendar
import matplotlib.pyplot as plt
from matplotlib.pylab import *
import matplotlib as mpl
from matplotlib import colors, colorbar
import pprint
import seaborn as sns
import pickle
import matplotlib.gridspec as gridspec
import os


def getMonthNumFromStr(monthString):
    if monthString == 'Janvier':
        return 1
    if monthString == 'Février':
        return 2
    if monthString == 'Mars':
        return 3
    if monthString == 'Avril':
        return 4
    if monthString == 'Mai':
        return 5
    if monthString == 'Juin':
        return 6
    if monthString == 'Juillet':
        return 7
    if monthString == 'Août':
        return 8
    if monthString == 'Septembre':
        return 9
    if monthString == 'Octobre':
        return 10
    if monthString == 'Novembre':
        return 11
    if monthString == 'Décembre':
        return 12

    return 0




def getMonthStr(monthNum):
    if monthNum == 1:
        return "Janvier"
    if monthNum ==2:
        return "Février"
    if monthNum == 3:
        return "Mars"
    if monthNum == 4:
        return "Avril"
    if monthNum == 5:
        return "Mai"
    if monthNum == 6:
        return "Juin"
    if monthNum == 7:
        return "Juillet"
    if monthNum == 8:
        return "Août"
    if monthNum == 9:
        return "Septembre"
    if monthNum == 10:
        return "Octobre"
    if monthNum == 11:
        return "Novembre"
    if monthNum == 12:
        return "Décembre"

    return 0


def removeDays(dayslist, daysToRemove):
    """Remove days from the dayslist"""
    return [j for j in dayslist if j not in daysToRemove]


class Famille:

    def __init__(self, nom):
        self.nom = nom
        self.total_voulu=2
        self.month_total=0
        self.color = '#ffffff'
        self.maxPermPerWeek=2
        self.dispos=[]
        self.total=0

    def set_color(self , colorstring):
        self.color = colorstring

    def set_nom(self, nom):
        self.nom=nom

    def set_total_voulu(self, n):
        self.total_voulu = n

    def set_month_total(self, n):
        self.month_total = n

    def set_total(self, total):
        self.total = total

    def get_total (self):
        return self.total

    def get_month_total(self):
        return self.month_total

    def set_colorNumber(self, colorNumber):
        self.colorNumber = colorNumber

    def add_dispos(self, dispos):
        self.dispos = np.append(self.dispos, dispos)

    def remove_dispos(self, dispos_to_remove):
        self.dispos = removeDays(self.dispos, dispos_to_remove)

    def set_dispos(self, dispos):
        self.dispos=dispos


    def setMaxPermPerWeek(self, n):
        self.maxPermPerWeek = n

    def clear_dispos(self):
        self.dispos = []

    def display(self):
        print('Famille : ', self.nom)
        print('Disponibilites : ', self.dispos)
        print('Total : ', self.total)
        print('Total voulu : ', self.total_voulu)


def moisToString(mois):
    if mois == 1:
        return 'Janvier'
    if mois == 2:
        return 'Février'
    if mois == 3:
        return 'Mars'
    if mois == 4:
        return 'Avril'
    if mois == 5:
        return 'Mai'
    if mois == 6:
        return 'Juin'
    if mois == 7:
        return 'Juillet'
    if mois == 8:
        return 'Août'
    if mois == 9:
        return 'Septembre'
    if mois == 10:
        return 'Octobre'
    if mois == 11:
        return 'Novembre'
    if mois == 12:
        return 'Décembre'


class Planning:

    def __init__(self):
        self.familles = []
        self.planning_open=dict()
        self.planning_affect=dict()
        self.fixed = dict()
        self.index=1
        self.nJours = 0
        self.nFamilles = 0
        self.nLibre= 0
        self.joursLibres = []
        self.jours_fermes = []
        self.cmapList= ["#DCDCDC"]
        self.cmap= mpl.colors.ListedColormap(self.cmapList)
        self.joursDeLaMemeSemaine = dict()
        self.semaineDeJour = dict()
        self.noConsecutivePerms = False
        self.famHasDisp=[]

    def avoidConsecutivePerms(self):
        self.noConsecutivePerms = True

    def set_mois(self, mois):
        self.mois=mois
        self.title = 'Planning Grands ' + moisToString(self.mois)

    def set_fermes(self):

        if self.mois == 9:
            self.jours_fermes = [1]
        if self.mois == 11:
            self.jours_fermes = [1]
        if self.mois == 12:
            self.jours_fermes = [25,26,27,28,29]
        if self.mois == 1:
            self.jours_fermes = [1]
        if self.mois == 4:
            self.jours_fermes = [2]
        if self.mois == 5:
            self.jours_fermes = [1,8,10,11, 21]
        if self.mois == 7:
            self.jours_fermes = [25,26,27,30,31]


    def set_annee(self, annee):
        self.annee=annee

    def addFerme(self, day):
        if day not in self.jours_fermes:
            self.jours_fermes.append(day)

    def removeFerme(self, day):
        if day in self.jours_fermes:
            self.jours_fermes = removeDays(self.jours_fermes, [day] )

    def calcul_calendrier(self):

        weekdays = [calendar.MONDAY, calendar.TUESDAY, calendar.WEDNESDAY, calendar.THURSDAY, calendar.FRIDAY]
        self.permJours = []
        self.permSemJours = []
        self.weeks=[]
        self.cal = calendar.monthcalendar(self.annee, self.mois)

        for j in range(len(self.cal)):
            for weekday in weekdays:
                if not (self.cal[j][weekday] in self.jours_fermes) and self.cal[j][weekday] != 0:
                    self.permJours.append(self.cal[j][weekday])
                    self.permSemJours.append(weekday)

        for i in self.permJours:
            self.semaineDeJour[i]=[]


        for i in range(len(self.cal)):
            self.weeks.append(self.cal[i])
            for k in range(len(self.cal[i])):
                if(self.cal[i][k] in self.permJours):

                    self.semaineDeJour[self.cal[i][k]].append(i)

        self.lundis = []
        self.mardis = []
        self.mercredis = []
        self.jeudis = []
        self.vendredis = []
        for j in range(len(self.permJours)):
            self.joursLibres.append(self.permJours[j])
            if not self.permJours[j] in self.jours_fermes:
                if self.permSemJours[j] == 0:
                    self.lundis.append(self.permJours[j])
                if self.permSemJours[j] == 1:
                    self.mardis.append(self.permJours[j])
                if self.permSemJours[j] == 2:
                    self.mercredis.append(self.permJours[j])
                if self.permSemJours[j] == 3:
                    self.jeudis.append(self.permJours[j])
                if self.permSemJours[j] == 4:
                    self.vendredis.append(self.permJours[j])
        for j in self.permJours:
            self.joursDeLaMemeSemaine[j] = []
            if not (j in self.jours_fermes):
                self.planning_open[j] = []
                self.planning_affect[j]=[]
            self.fixed[j]=False
            for k in self.permJours:
                if self.semaineDeJour[k] == self.semaineDeJour[j] and not(j==k):
                    self.joursDeLaMemeSemaine[j].append(k)
        self.joursLibres=removeDays(self.joursLibres, self.jours_fermes)
        self.nJours = len(self.permJours)-len(self.jours_fermes)
        self.nLibre = len(self.joursLibres)


    def add_Famille(self, famille):
        self.familles.append(famille)
        self.cmapList.append(famille.color)
        self.cmap= mpl.colors.ListedColormap(self.cmapList)
        self.nFamilles +=1

        famille.set_colorNumber(self.index)
        self.index += 1

    def add_familles_dispos(self):

        for fam in self.familles:
            if len(fam.dispos)>0:
                for i in fam.dispos:
                    self.planning_open[i].append(fam)
                self.famHasDisp.append(fam)


    def display_familles(self):

        for i in range(len(self.familles)):
            f = self.familles[i]
            f.display()
            print()

    def display(self, savefig=''):
        self.display_cal(self.planning_affect, savefig)

    def display_cal(self, planning, savefig=''):

        weeksIndex = np.arange(len(self.weeks)) +1
        weeksLabels= []
        for i in weeksIndex:
            weeksLabels.append("Semaine " + str(i))
        daysIndex = np.arange(7)
        nWeeks = len(weeksIndex)

        # Define colors
        self.cmap.set_over('0.25')
        self.cmap.set_under('0.75')
        bounds = np.arange(self.nFamilles+2)
        # bounds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        norm = mpl.colors.BoundaryNorm(bounds, self.cmap.N)

        # Plot
        fig = plt.figure(figsize=(13, 8))

        fig.suptitle(self.title , fontsize=18, fontweight='bold')
        gs = gridspec.GridSpec(1, 5)

        ax2 = plt.subplot(gs[:, 4:])
        cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=self.cmap,
                                        norm=norm,
                                        # to use 'extend', you must
                                        # specify two extra boundaries:
                                        # boundaries=[0] + bounds + [13],
                                        # extend='both',
                                        ticks=bounds,  # optional
                                        spacing='proportional',
                                        orientation='vertical')
        plt.setp(ax2.get_yticklabels(), visible=False)

        for k in range(len(self.familles)):
            plt.text(0.05, 0.12 + k * 0.09 , self.familles[k].nom, fontsize=14, color='k')
            plt.text(1.35, 0.12 + k * 0.09, self.familles[k].get_total(), fontsize=14, color='k')
        plt.text(0.05, 0.08 + (-0.5) * 0.091, 'N/A', fontsize=14, color='k')
        plt.text(1.3, 1.05, 'Total', fontsize=14, color='k')
        plt.text(1.29, 1.03, 'annuel', fontsize=14, color='k')

        image = np.zeros(7 * nWeeks)
        image = image.reshape((nWeeks, 7))
        ax1 = plt.subplot(gs[:, :4])
        for i in range(nWeeks):
            for j in range(7):
                dayNum = self.cal[i][j]
                image[i][j] = 0
                if dayNum in planning.keys():
                    if len(planning[dayNum])>0:
                        image[i][j] = planning[dayNum][0].colorNumber

        row_labels = weeksLabels
        col_labels = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']
        ax1.matshow(image, cmap=self.cmap,vmin=0, vmax=self.nFamilles)
        ax1.grid(False)
        ax1.set_xticks(range(7))
        ax1.set_xticklabels( col_labels,fontsize=14)
        ax1.set_yticks(range(nWeeks))
        ax1.set_yticklabels(row_labels,fontsize=14)


        # Make grid
        vgrid = []
        for i in range(7 + 1):
            vgrid.append((i - 0.5, i - 0.5))
            vgrid.append(( - 0.5, nWeeks - 0.5))
        hgrid = []
        for i in range(nWeeks + 1):
            hgrid.append((- 0.5, 6.5))
            hgrid.append((i - 0.5, i - 0.5))
        for i in range(nWeeks+1):
            ax1.plot(hgrid[2 * i], hgrid[2 * i + 1], 'k-')
        for i in range(7 + 1):
            ax1.plot(vgrid[2 * i], vgrid[2 * i + 1], 'k-')

        # add labels
        for i in range(nWeeks):
            for j in range(7):
                dayNum = self.cal[i][j]
                if dayNum !=0:
                    ax1.text(j+0.2, i-.3, str(dayNum), fontsize=14, color='k')
                if dayNum in planning:
                    k=0
                    nF=len(planning[dayNum])
                    for fam in planning[dayNum]:
                        ax1.text(j-0.3 , i + 0.5*float(k/nF) , fam.nom, fontsize=14, color='k')
                        k+=1
        if savefig:
            plt.savefig(os.getcwd() + '/images/' + savefig + '.pdf')
        plt.show()

    def fixday(self, day):
        if day in self.permJours:
            self.fixed[day]=True

    def freeDay(self, day):

        if day in self.planning_open.keys():

            if len(self.planning_affect[day])>0:
                for fam in self.planning_affect[day]:
                    fam.set_total(fam.get_total() - 1)
                    fam.set_month_total(fam.get_month_total() - 1)
                del self.planning_affect[day][0]
                self.planning_open[day] = []
                self.joursLibres.append(day)
                self.nLibre = len(self.joursLibres)

    def forcePerm(self, day, famille):
        if famille not in self.familles:
            self.freeDay(day)
        self.fixday(day)
        self.assignPerm(day, famille)

    def assignPerm(self, day, famille):
        dayList=[]
        dayList.append(day)
        if famille in self.familles:
            if len(self.planning_affect[day])>0:
                f = self.planning_affect[day][0]
                f.set_total(f.get_total()-1)
                f.set_month_total(f.get_month_total()-1)

                del self.planning_affect[day][0]
            self.planning_affect[day].append(famille)
            famille.set_total(famille.get_total()+1)
            famille.set_month_total(famille.get_month_total() + 1)

            self.joursLibres = removeDays(self.joursLibres, dayList)
            self.nLibre = len(self.joursLibres)

        else:
            raise ValueError ('La famille ', famille.nom,' n est pas dans le planning')


    def set_easyPerm(self):
        for i in self.permJours:
            if len(self.planning_open[i])==1:
                self.assignPerm(i,self.planning_open[i][0])



    def monte_carlo(self, nIt=1):
        n=0
        while n<nIt :
            tries = 0
            i = np.random.choice(self.permJours)
            if not self.fixed[i] and not i in self.jours_fermes:

                while tries < 10:
                    if self.planning_open[i]==[]:
                        raise ValueError('Aucune famille disponible le {}'.format(i))
                        return
                    f = np.random.choice(self.planning_open[i])
                    tagOK = True

                    # check if family f has already enough perm during the week of day #i
                    count = 0
                    for k in self.joursDeLaMemeSemaine[i]:
                        if len(self.planning_affect[k])>0 and self.planning_affect[k][0]==f :
                            count+=1
                    if count>f.maxPermPerWeek-1:
                        tagOK=False

                    if self.noConsecutivePerms:
                        if i-1 in self.joursDeLaMemeSemaine[i] and len(self.planning_affect[i-1])>0:
                            if self.planning_affect[i-1][0]==f:
                                tagOK=False
                        if i + 1 in self.joursDeLaMemeSemaine[i] and len(self.planning_affect[i + 1]) > 0:
                            if self.planning_affect[i + 1][0] == f:
                               tagOK = False

                    # check if family f has already enough perm during the week of day #i
                    count = 0
                    for k in self.joursDeLaMemeSemaine[i]:
                        if len(self.planning_affect[k])>0 and self.planning_affect[k][0]==f :
                            count+=1
                    if count>f.maxPermPerWeek-1:
                        tagOK=False

                    # authorize change only if the number of errors decreases
                    if tagOK and len(self.planning_affect[i]) > 0:
                        old_fam = self.planning_affect[i][0]
                        deltaErr= -np.abs(old_fam.total_voulu - (old_fam.get_month_total())) + np.abs(old_fam.total_voulu - (old_fam.get_month_total()-1)) + np.abs(f.total_voulu - (f.get_month_total()+1)) - np.abs(f.total_voulu - (f.get_month_total()))
                        if deltaErr > 0:
                            tagOK=False

                    if tagOK:
                        self.assignPerm(i,f)
                        break
                    else:
                        tries+=1
                    n += 1

            nErr=0
            for fam in self.familles:

                if fam.total_voulu != fam.get_month_total() and fam in self.famHasDisp:
                    nErr+= np.abs(fam.total_voulu - fam.get_month_total())

            print("nErr= ", nErr)
            if self.nLibre == 0 and nErr<2:
                break

        print(n,'iterations performed.')
        for fam in self.familles:
            if fam.total_voulu != fam.get_month_total() and fam in self.famHasDisp:
                print(fam.nom, fam.total_voulu , fam.get_month_total())

    def save(self, nom):
        pickle.dump(self, open(os.getcwd() + '/saved/' + nom + '.pl', 'wb'))
        return

    def getFamille(self, nom):
        for fam in self.familles:
            if fam.nom == nom:
                return fam


    def importLastPlanning(self, filename):

        try:
            filename.endswith('.pl')
        except IOError:
            print("wrong file format")

        lastPlanning = pickle.load((open(os.getcwd() + '/saved/' + filename, 'rb')))
        k=0
        for fam in lastPlanning.familles:
            self.add_Famille(fam)
            self.familles[k].clear_dispos()
            self.familles[k].set_total_voulu(2)
            self.familles[k].set_month_total(0)
            k+=1

        self.cmap = lastPlanning.cmap

def importCurrentPlanning(filename):

    try:
        filename.endswith('.pl')
    except IOError:
        print("wrong file format")
    currPlan=pickle.load((open(os.getcwd() + '/saved/' + filename, 'rb')))
    return currPlan


if __name__ == "__main__":

    # planningJuillet=Planning()
    # planningJuillet.set_annee(2018)
    # planningJuillet.set_mois(7)
    # planningJuillet.set_fermes()
    # # planningMar.avoidConsecutivePerms()
    # planningJuillet.importLastPlanning('juin2018.pl')
    # planningJuillet.calcul_calendrier()
    #
    #
    # allDays=planningJuillet.permJours
    #
    # dispNora = [10,11,17,18]
    # planningJuillet.getFamille('Nora').add_dispos(dispNora)
    # planningJuillet.getFamille('Nora').set_total_voulu(2)
    #
    # dispSeymour = [3,4]
    # planningJuillet.getFamille('Seymour').add_dispos(dispSeymour)
    # planningJuillet.getFamille('Seymour').set_total_voulu(2)
    #
    #
    # dispPaul= [11,13,18]
    # planningJuillet.getFamille('Paul').add_dispos(dispPaul)
    # planningJuillet.getFamille('Paul').set_total_voulu(2)
    #
    #
    # dispLisa = [2,5]
    # planningJuillet.getFamille('Lisa').add_dispos(dispLisa)
    # planningJuillet.getFamille('Lisa').set_total_voulu(2)
    # # planningApr.getFamille('Lisa').setMaxPermPerWeek(2)
    #
    # dispLucien = [ 2, 3, 10, 12, 16, 23, 24]
    # planningJuillet.getFamille('Lucien').add_dispos(dispLucien)
    # planningJuillet.getFamille('Lucien').set_total_voulu(2)
    #
    # dispSacha=[]
    # planningJuillet.getFamille('Sacha').add_dispos(dispSacha)
    # planningJuillet.getFamille('Sacha').set_total_voulu(0)
    # planningJuillet.getFamille('Sacha').setMaxPermPerWeek(2)
    #
    # # dispLola=removeDays(allDays, [4])
    # planningJuillet.getFamille('Lola').add_dispos(allDays)
    # planningJuillet.getFamille('Lola').set_total_voulu(0)
    #
    #
    # dispZelie=[11,19,20]
    # planningJuillet.getFamille('Zelie').add_dispos(dispZelie)
    # planningJuillet.getFamille('Zelie').set_total_voulu(2)
    # planningJuillet.getFamille('Zelie').setMaxPermPerWeek(1)
    #
    # dispNour= allDays
    # planningJuillet.getFamille('Nour').add_dispos(dispNour)
    # planningJuillet.getFamille('Nour').setMaxPermPerWeek(1)
    # planningJuillet.getFamille('Nour').set_total_voulu(1)
    #
    #
    # dispMaryem=[6,9]
    # planningJuillet.getFamille('Maryem').add_dispos(dispMaryem)
    # planningJuillet.getFamille('Maryem').set_total_voulu(2)
    #
    # planningJuillet.add_familles_dispos()
    # planningJuillet.set_easyPerm()
    #
    # planningJuillet.forcePerm(10, planningJuillet.getFamille('Nora'))
    #
    # planningJuillet.forcePerm(11, planningJuillet.getFamille('Zelie'))
    # planningJuillet.forcePerm(20, planningJuillet.getFamille('Zelie'))
    #
    # planningJuillet.forcePerm(3, planningJuillet.getFamille('Seymour'))
    # planningJuillet.forcePerm(4, planningJuillet.getFamille('Seymour'))

    # planningJuillet.getFamille('Lola').set_total(planningJuillet.getFamille('Lola').get_total() + 0.5)
    # planningJuillet.getFamille('Paul').set_total(planningJuillet.getFamille('Paul').get_total() + 0.5)
    # planningJuillet.getFamille('Sacha').set_total(planningJuillet.getFamille('Sacha').get_total() - 0.5)
    # planningJuillet.getFamille('Nora').set_total(planningJuillet.getFamille('Nora').get_total() - 1)
    # planningJuillet.getFamille('Maryem').set_total(planningJuillet.getFamille('Maryem').get_total() + 0.5)

    planningJuillet = importCurrentPlanning('juillet2018.pl')
    planningJuillet.forcePerm(24, planningJuillet.getFamille('Lucien'))

    planningJuillet.forcePerm(3, planningJuillet.getFamille('Sacha'))
    planningJuillet.forcePerm(10, planningJuillet.getFamille('Nora'))
    planningJuillet.forcePerm(19, planningJuillet.getFamille('Nour'))
    planningJuillet.forcePerm(12, planningJuillet.getFamille('Maryem'))

    planningJuillet.save('juillet2018')
    planningJuillet.display('juillet2018')

