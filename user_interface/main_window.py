import user_interface_toolbox

import hub_ui
import new_planning_ui
import current_planning_ui
import last_planning_ui

import os

from PyQt4 import QtGui


class mainWindow(user_interface_toolbox.mainWindow):
    """
    @brief		Main window for the streaming, hub and log user interfaces.
    """

    def setParameters(self):
        self.setWindowTitle('Planning des permanences')
        # set the application icon
        image = os.path.join(os.path.dirname(__file__), 'images/logo-lcd.jpg')
        self.app.setWindowIcon(QtGui.QIcon(os.path.abspath(image)))
        self.central_widget = QtGui.QStackedWidget(parent=self)
        self.setCentralWidget(self.central_widget)

        hub_widget = hub_ui.mainHub(self)
        self.setGeometry(50, 50, 600, 200)
        self.central_widget.addWidget(hub_widget)

    def set_new_planning(self):
        """
		@brief		Define the streaming UI as central widget to show.
		"""
        self.setGeometry(50, 50, 600, 200)
        new_widget = new_planning_ui.NewPlanningUI(self)
        self.central_widget.addWidget(new_widget)
        self.central_widget.setCurrentWidget(new_widget)
        self.centerWindow()

    def set_new_planning_cal(self):
        """
        @brief		Define the streaming UI as central widget to show.
        """

        self.setGeometry(50, 50, 1300, 850)
        new_widget = new_planning_ui.NewPlanningCalUI(self)
        self.central_widget.addWidget(new_widget)
        self.central_widget.setCurrentWidget(new_widget)
        self.centerWindow()

    def set_new_planning_family_sel(self):
        """
        @brief		Define the streaming UI as central widget to show.
        """
        self.setGeometry(50, 50, 1300, 850)
        new_widget = new_planning_ui.NewPlanning_FamUI(self)
        self.central_widget.addWidget(new_widget)
        self.central_widget.setCurrentWidget(new_widget)
        self.centerWindow()


    def set_current_planning(self):
        """
		@brief		Define the Log UI as central widget to show.
		"""
        self.setWindowTitle('Sélectionner les jours de fermeture de la crèche (hors week end)')
        self.setGeometry(50, 50, 1400, 900)
        current_widget = new_planning_ui.NewPlanningUI(self)
        self.central_widget.addWidget(current_widget)
        self.central_widget.setCurrentWidget(current_widget)
        self.centerWindow()

    def set_last_planning(self):
        """
			@brief		Define the Log UI as central widget to show.
		"""
        self.setGeometry(50, 50, 1400, 900)
        last_widget = new_planning_ui.NewPlanningUI(self)
        self.central_widget.addWidget(last_widget)
        self.central_widget.setCurrentWidget(last_widget)
        self.centerWindow()

    def set_hub(self):
        """
		@brief		Define the Hub UI as central widget to show.
		"""
        self.setGeometry(50, 50, 600, 200)
        hub_widget = hub_ui.mainHub(self)
        self.central_widget.addWidget(hub_widget)
        self.central_widget.setCurrentWidget(hub_widget)
        self.centerWindow()

    def setMonth(self, month):
        self.month=month

    def setYear(self, year):
        self.year=year

    def setPlanning(self, plan):
        self.monthPlanning = plan
