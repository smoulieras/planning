import user_interface_toolbox

import sys
import os
import csv
import datetime
import sys
sys.path.append('../')
import planning as pl


from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtCore import pyqtSlot

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
plt.style.use('ggplot')

def makeCalTableFromPlanning(monthPlanning):

	calTable = QTableWidget()
	monthStr = pl.getMonthStr(monthPlanning.mois)

	calTable.setRowCount(len(monthPlanning.weeks))
	calTable.setColumnCount(7)
	calTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
	calTable.setSelectionMode(QtGui.QAbstractItemView.NoSelection)

	headers = ["Lundi", "Mardi", "Mercredi",
			   "Jeudi", "Vendredi", "Samedi", "Dimanche"]
	calTable.setHorizontalHeaderLabels(headers)
	calTable.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
	calTable.verticalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
	return calTable



class NewPlanning_FamUI(user_interface_toolbox.userInterface):

	def createUI(self):
		self.monthPlanning = self.parent.monthPlanning
		self.monthStr = pl.getMonthStr(self.parent.month)
		self.famPlanCal = dict()

		font = QtGui.QFont()
		font.setPointSize(11)
		self.btn_quit = QtGui.QPushButton('Quitter')
		self.btn_quit.clicked.connect(self.close)
		self.btn_quit.setFont(font)

		self.createButton = QPushButton("Nouvelle famille")
		self.createButton.clicked.connect(self.addFamName)

		self.listOfFam = QtGui.QListWidget(self)
		self.listOfFam.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

		# self.setColor = QtGui.QPushButton(self)
		# self.setColor.setText("Choisir une couleur")
		# self.setColor.clicked.connect(self.fam_color)
		# self.setColor.setEnabled(False)

		self.setTotalVoulu = QLineEdit()
		self.setTotalVoulu.setValidator(QIntValidator())
		self.setTotalVoulu.setMaxLength(4)
		self.setTotalVoulu.setInputMask('5')
		self.setTotalVoulu.setAlignment(Qt.AlignRight)
		self.setTotalVoulu.setFont(QFont("Arial", 14))

		self.setPretotal = QLineEdit()
		self.setPretotal.setValidator(QIntValidator())
		self.setPretotal.setMaxLength(4)
		self.setPretotal.setAlignment(Qt.AlignRight)
		self.setPretotal.setFont(QFont("Arial", 14))

		self.editBox = QtGui.QGroupBox("Édition")
		self.editBox.setEnabled(False)

		vbox = QtGui.QFormLayout()
		vbox.addRow("Nombre de permanences souhaitées pour ce mois", self.setTotalVoulu)
		vbox.addRow("Total de permanences déjà effectuées", self.setPretotal)

		# vbox.addWidget()

		self.editBox.setLayout(vbox)

		self.urLayout = QtGui.QVBoxLayout()
		self.urLayout.addStretch()
		self.urLayout.addWidget(self.createButton)
		self.urLayout.addItem(QtGui.QSpacerItem(0, 100))
		self.urLayout.addWidget(self.editBox)
		self.urLayout.addStretch()


		self.grayBrush = QtGui.QBrush(QColor("gray"))
		self.grayBrush.setStyle(QtCore.Qt.SolidPattern)

		self.greenBrush = QtGui.QBrush(QColor("lightgreen"))
		self.greenBrush.setStyle(QtCore.Qt.SolidPattern)

		self.orangeBrush = QtGui.QBrush(QColor("orange"))
		self.orangeBrush.setStyle(QtCore.Qt.SolidPattern)

		self.cal_hbox = QtGui.QHBoxLayout()

		# initiate table
		self.planningCal= QStackedWidget()
		self.planningCal.addWidget(makeCalTableFromPlanning(self.monthPlanning))

		self.listOfFam.itemClicked.connect(self.fam_click)

		self.cal_hbox.addWidget(self.planningCal)

		full_vbox = QtGui.QVBoxLayout()
		hbox_famEdit = QtGui.QHBoxLayout()
		hbox_famEdit.addWidget(self.listOfFam)
		hbox_famEdit.addItem(QtGui.QSpacerItem(100,0))
		hbox_famEdit.addLayout(self.urLayout)

		full_vbox.addLayout(hbox_famEdit)
		full_vbox.addLayout(self.cal_hbox)

		self.setLayout(full_vbox)

# 	def fam_color(self):
	# 	famName = self.listOfFam.currentItem().text()
	# 	self.planningCal.setCurrentWidget(self.famPlanCal[famName])
	# 	self.setColor.setEnabled(True)
	# 	self.editFamDisp()

	def fam_pretotal(self):
		famName = self.listOfFam.currentItem().text()
		num, ok = QInputDialog.getInt(self, 'Prétotal ' + famName, "Entrer le total de permanences déjà effectuées", self.monthPlanning.getFamille(famName).total)
		if ok and num!=None:
			self.monthPlanning.getFamille(famName).set_total(num)

	def fam_totalVoulu(self):
		famName = self.listOfFam.currentItem().text()
		num, ok = QInputDialog.getInt(self, 'Total ' + famName + " pour ce mois", "Entrer le total de permanences voulu pour ce mois", self.monthPlanning.getFamille(famName).total_voulu)
		if ok and num != None:
			self.monthPlanning.getFamille(famName).set_total_voulu(num)

	def fam_click(self):
		famName = self.listOfFam.currentItem().text()
		self.planningCal.setCurrentWidget(self.famPlanCal[famName])
		self.editBox.setEnabled(True)
		self.editFamDisp()

	def editFamDisp(self):
		famName = self.listOfFam.currentItem().text()

		for i in range(len(self.monthPlanning.weeks)):
			for j in range(7):
				dayNum = self.monthPlanning.cal[i][j]
				if dayNum in self.monthPlanning.permJours:
					if dayNum in self.monthPlanning.getFamille(famName).dispos:
						self.famPlanCal[famName].setItem(i, j, QtGui.QTableWidgetItem(
							str(dayNum) + " " + self.monthStr + " : dispo"))
						self.famPlanCal[famName].item(i, j).setBackground(self.greenBrush)
					else:
						self.famPlanCal[famName].setItem(i, j, QtGui.QTableWidgetItem(
							str(dayNum) + " " + self.monthStr + " : pas dispo"))
						self.famPlanCal[famName].item(i, j).setBackground(self.orangeBrush)
				elif dayNum == 0:
					self.famPlanCal[famName].setItem(i, j, QTableWidgetItem('N/A'))
					self.famPlanCal[famName].item(i, j).setBackground(self.grayBrush)
				else:
					self.famPlanCal[famName].setItem(i, j, QTableWidgetItem(str(dayNum) + " " + self.monthStr + " : fermé"))
					self.famPlanCal[famName].item(i, j).setBackground(self.grayBrush)

		# self.cal_hbox.addWidget(self.famPlanCal[famName])
		self.famPlanCal[famName].cellClicked.connect(self.swapDisp)


	def addFamName(self):
		text, ok = QInputDialog.getText(self, 'Nouvelle famille', 'Entrer le nom de la nouvelle famille:')
		if text !="" and ok:
			newFam=pl.Famille(text)
			self.monthPlanning.add_Famille(newFam)
			item = QtGui.QListWidgetItem(self.listOfFam)
			item.setText(text)
			self.listOfFam.addItem(item)
			self.famPlanCal[text] = makeCalTableFromPlanning(self.monthPlanning)
			self.planningCal.addWidget(self.famPlanCal[text])


	@QtCore.pyqtSlot(int, int)
	def swapDisp(self, ligne, colonne):
		dayNum = self.monthPlanning.cal[ligne][colonne]
		famName= self.listOfFam.currentItem().text()
		disp= self.monthPlanning.getFamille(famName).dispos
		if dayNum not in disp and dayNum in self.monthPlanning.permJours:
			self.monthPlanning.getFamille(famName).add_dispos([dayNum])
			self.famPlanCal[famName].setItem(ligne, colonne, QTableWidgetItem(str(dayNum) + " " + self.monthStr + " : dispo"))
			self.famPlanCal[famName].item(ligne, colonne).setBackground(self.greenBrush)

		elif dayNum in disp and dayNum in self.monthPlanning.permJours:
			self.monthPlanning.getFamille(famName).remove_dispos([dayNum])
			self.famPlanCal[famName].setItem(ligne, colonne,
									   QTableWidgetItem(str(dayNum) + " " + self.monthStr + " : pas dispo"))
			self.famPlanCal[famName].item(ligne, colonne).setBackground(self.orangeBrush)


class NewPlanningCalUI(user_interface_toolbox.userInterface):

	def createUI(self):
		font = QtGui.QFont()
		font.setPointSize(11)
		self.parent.setWindowTitle("Selectionner les jours fermés (hors week end)")

		self.btn_quit = QtGui.QPushButton('Quitter')
		self.btn_quit.clicked.connect(self.close)
		self.btn_quit.setFont(font)

		self.btn_OK = QtGui.QPushButton('OK')
		self.btn_OK.clicked.connect(self.launch_family_selection)
		self.btn_OK.setFont(font)

		self.monthPlanning = pl.Planning()
		self.monthPlanning.set_mois(self.parent.month)
		self.monthPlanning.set_annee(self.parent.year)
		self.monthPlanning.set_fermes()
		self.monthPlanning.calcul_calendrier()

		self.monthStr = pl.getMonthStr(self.parent.month)

		self.grayBrush = QtGui.QBrush(QColor("gray"))
		self.grayBrush.setStyle(QtCore.Qt.SolidPattern)

		self.greenBrush = QtGui.QBrush(QColor("lightgreen"))
		self.greenBrush.setStyle(QtCore.Qt.SolidPattern)

		# initiate table
		self.emptyCalTable = makeCalTableFromPlanning(self.monthPlanning)

		for i in range(len(self.monthPlanning.weeks)):
			for j in range(7):
				dayNum = self.monthPlanning.cal[i][j]
				if dayNum in self.monthPlanning.planning_open.keys():
					self.emptyCalTable.setItem(i, j, QtGui.QTableWidgetItem(str(dayNum) + " " + self.monthStr + " : ouvert"))
					self.emptyCalTable.item(i, j).setBackground(self.greenBrush)
				elif dayNum == 0:
					self.emptyCalTable.setItem(i, j, QTableWidgetItem('N/A'))
					self.emptyCalTable.item(i, j).setBackground(self.grayBrush)
				else:
					self.emptyCalTable.setItem(i, j, QTableWidgetItem(str(dayNum) + " " + self.monthStr + " : Week End"))
					self.emptyCalTable.item(i, j).setBackground(self.grayBrush)

		self.emptyCalTable.cellClicked.connect(self.swapOpenClose)

		full_vbox = QtGui.QVBoxLayout()
		cal_hbox = QtGui.QHBoxLayout()
		cal_hbox.addWidget(self.emptyCalTable)


		hbox_quit = QtGui.QHBoxLayout()
		hbox_quit.addStretch(1)
		hbox_quit.addWidget(self.btn_OK)
		hbox_quit.addItem(QtGui.QSpacerItem(200, 0))
		hbox_quit.addWidget(self.btn_quit)
		hbox_quit.addStretch(1)

		full_vbox.addLayout(cal_hbox)
		full_vbox.addItem(QtGui.QSpacerItem(0, 50))
		full_vbox.addLayout(hbox_quit)

		self.setLayout(full_vbox)


	@QtCore.pyqtSlot(int,int)
	def swapOpenClose(self, ligne, colonne):
		dayNum = self.monthPlanning.cal[ligne][colonne]
		if dayNum not in self.monthPlanning.jours_fermes and dayNum in self.monthPlanning.permJours:
			self.monthPlanning.addFerme(dayNum)
			self.emptyCalTable.setItem(ligne, colonne, QTableWidgetItem(str(dayNum) +" "+ self.monthStr + " : fermé") )
			self.emptyCalTable.item(ligne, colonne).setBackground(self.grayBrush)

		elif dayNum in self.monthPlanning.jours_fermes and dayNum in self.monthPlanning.permJours:
			self.monthPlanning.removeFerme(dayNum)
			self.emptyCalTable.setItem(ligne, colonne, QTableWidgetItem(str(dayNum) +" "+ self.monthStr + " : ouvert"))
			self.emptyCalTable.item(ligne, colonne).setBackground(self.greenBrush)

	def launch_family_selection(self):
		self.monthPlanning.calcul_calendrier()
		self.parent.setPlanning(self.monthPlanning)
		self.parent.set_new_planning_family_sel()


class NewPlanningUI(user_interface_toolbox.userInterface):
	"""
	@brief		User Interface to visualise recorded streaming sessions.
	"""

	def createUI(self):
		"""
		@brief		Methods to set up the UI widgets.
		"""

		# === interactive panel === #
		# icon
		label = QtGui.QLabel()
		image_path = os.path.join(sys.path[0], 'images/logo-lcd.jpg')
		image = QtGui.QPixmap(image_path)
		scaled_image = image.scaled(450, 450, Qt.KeepAspectRatio)
		label.setPixmap(scaled_image)

		font = QtGui.QFont()
		font.setPointSize(11)

		self.btn_quit = QtGui.QPushButton('Quitter')
		self.btn_quit.clicked.connect(self.close)
		self.btn_quit.setFont(font)


		todaysYear = int(datetime.datetime.now().year)

		self.yearlist_app = QtGui.QComboBox()
		self.yearlist_app.setFont(font)
		for i in range(5):
			self.yearlist_app.addItem(str(todaysYear + i))

		self.monthlist_app = QtGui.QComboBox()
		self.monthlist_app.setFont(font)
		self.monthlist_app.addItem('Janvier')
		self.monthlist_app.addItem('Février')
		self.monthlist_app.addItem('Mars')
		self.monthlist_app.addItem('Avril')
		self.monthlist_app.addItem('Mai')
		self.monthlist_app.addItem('Juin')
		self.monthlist_app.addItem('Juillet')
		self.monthlist_app.addItem('Août')
		self.monthlist_app.addItem('Septembre')
		self.monthlist_app.addItem('Octobre')
		self.monthlist_app.addItem('Novembre')
		self.monthlist_app.addItem('Décembre')

		btn_launch = QtGui.QPushButton('OK')
		btn_launch.setFont(font)
		btn_launch.clicked.connect(self.launch_new_planning_cal)

		full_vbox = QtGui.QVBoxLayout()
		hbox_icon = QtGui.QHBoxLayout()
		hbox_icon.addStretch(1)
		hbox_icon.addWidget(label)
		hbox_icon.addStretch(1)

		hub_hbox2 = QtGui.QHBoxLayout()
		hub_hbox2.addStretch(1)
		hub_hbox2.addWidget(self.yearlist_app)
		hub_hbox2.addStretch(1)
		hub_hbox2.addWidget(self.monthlist_app)
		hub_hbox2.addStretch(1)
		hub_hbox2.addWidget(btn_launch)
		hub_hbox2.addStretch(1)

		hbox_quit = QtGui.QHBoxLayout()
		hbox_quit.addStretch(1)
		hbox_quit.addWidget(self.btn_quit)
		hbox_quit.addStretch(1)

		full_vbox.addStretch(1)
		full_vbox.addLayout(hbox_icon)
		full_vbox.addStretch(1)
		full_vbox.addLayout(hub_hbox2)
		full_vbox.addStretch(1)
		full_vbox.addLayout(hbox_quit)
		full_vbox.addStretch(1)

		self.setLayout(full_vbox)

	def launch_new_planning_cal(self):

		self.month = pl.getMonthNumFromStr(self.monthlist_app.currentText())
		self.year = int(self.yearlist_app.currentText())
		self.parent.setMonth(self.month)
		self.parent.setYear(self.year)
		self.parent.set_new_planning_cal()


	def set_year(self, year):
		self.year = year

	def set_month(self, month):
		self.month = month


	def show_file_dialog(self):
		"""
		@brief		Display a dialog box to select the .csv file to plot.
		"""
		start_browse = sys.path[0] + '/log_sessions/'
		fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', start_browse, '*.csv')
		# if name is '', it means that no file has been selected
		if fname != '':
			# display name of the selected file
			self.file_label.setText(fname)
			message = 'file ' + fname + ' selected'
			self.parent.printStatus(message)

			# create an axis
			self.ax_ppg = self.figure_ppg.add_subplot(111)
			self.ax_accel = self.figure_accel.add_subplot(111)

			# plot the file selected
			self.plot_file(fname)

		else:
			print('No file selected')
			self.parent.printStatus('No file selected')

	def plot_file(self, csv_file):
		"""
		@brief		Plot the .csv file selected.
		@param		csv_file (string): file containing the data to plot.
		"""
		timestamp_ppg = []
		ppg = []
		timestamp_accel = []
		accel = []

		tmp = open(csv_file, 'r', newline='')
		tmp_reader = csv.reader(tmp, delimiter=';')

		# skip header
		next(tmp_reader)

		for row in tmp_reader:
			ppg.append(float(row[1]))
			timestamp_ppg.append(datetime.datetime.strptime(str(row[0]), "%Y-%m-%d %H:%M:%S.%f"))
			if row[2] == '[]':
				pass
			else:
				accel.append(float(row[2]))
				timestamp_accel.append(datetime.datetime.strptime(str(row[0]), "%Y-%m-%d %H:%M:%S.%f"))

		# close file when finished
		tmp.close()

		# discards the old graph
		self.ax_ppg.clear()
		self.ax_accel.clear()

		# plot data
		self.ax_ppg.plot(timestamp_ppg, ppg, '-')
		self.ax_accel.plot(timestamp_accel, accel, '-')

		# refresh canvas
		self.canvas_ppg.draw()
		self.canvas_accel.draw()

		self.figure_ppg.tight_layout()
		self.figure_accel.tight_layout()

	def reset_plot(self):
		"""
		@brief		Erase the graphs and the file selected.
		"""
		self.file_label.setText('')

		# clear the figures
		self.figure_ppg.clear()
		self.figure_accel.clear()
		# refresh canvas
		self.canvas_ppg.draw()
		self.canvas_accel.draw()

		print('Reset done')
		self.parent.printStatus('Reset done')

	def do_check_accel(self):
		"""
		@brief		Method linked to the PPG checkbox.
		@details	Hide or show the PPG plot.
		"""
		if not self.check_accel.isChecked():
			self.canvas_accel.setHidden(True)
			self.toolbar_accel.setHidden(True)
		if self.check_accel.isChecked():
			self.canvas_accel.setHidden(False)
			self.toolbar_accel.setHidden(False)

	def do_check_ppg(self):
		"""
		@brief		Method linked to the PPG checkbox.
		@details	Hide or show the PPG plot.
		"""
		if not self.check_ppg.isChecked():
			self.canvas_ppg.setHidden(True)
			self.toolbar_ppg.setHidden(True)
		if self.check_ppg.isChecked():
			self.canvas_ppg.setHidden(False)
			self.toolbar_ppg.setHidden(False)




