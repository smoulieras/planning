import user_interface_toolbox

import sys
import os
import csv
import datetime

from PyQt4 import QtGui
from PyQt4.QtCore import Qt

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

plt.style.use('ggplot')


class LastPlanningUI(user_interface_toolbox.userInterface):
    """
    @brief		User Interface to visualise recorded streaming sessions.
    """

    def createUI(self):
        """
        @brief		Methods to set up the UI widgets.
        """
        # === interactive panel === #
        # icon
        label = QtGui.QLabel()
        image_path = os.path.join(sys.path[0], './images/logo.png')
        image = QtGui.QPixmap(image_path)
        scaled_image = image.scaled(168, 168, Qt.KeepAspectRatio)
        label.setPixmap(scaled_image)
        # chose file button
        self.btn_file = QtGui.QPushButton('Select file')
        self.btn_file.clicked.connect(self.show_file_dialog)
        self.btn_file.setMaximumSize(100, 100)
        # display name of selected file
        self.file_label = QtGui.QLineEdit('')
        self.file_label.setMinimumWidth(180)
        # set in read-only mode
        self.file_label.setReadOnly(True)
        # define the checkboxes
        self.check_ppg = QtGui.QCheckBox('PPG')
        self.check_accel = QtGui.QCheckBox('Accelerometer')
        # default is PPG and accel checked
        self.check_ppg.setChecked(True)
        self.check_accel.setChecked(True)
        # associate action to the checkboxes
        self.check_ppg.stateChanged.connect(self.do_check_ppg)
        self.check_accel.stateChanged.connect(self.do_check_accel)
        # quit application
        self.btn_quit = QtGui.QPushButton('QUIT')
        self.btn_quit.clicked.connect(self.close)
        # return to main hub menu
        self.btn_backtomenu = QtGui.QPushButton('Back to menu')
        self.btn_backtomenu.clicked.connect(self.parent.set_hub)
        # reset graphs and file button
        self.btn_reset = QtGui.QPushButton('Reset')
        self.btn_reset.clicked.connect(self.reset_plot)
        self.btn_reset.setMaximumSize(100, 100)
        # figure instances to plot on
        self.figure_ppg = Figure()
        self.figure_accel = Figure()
        # canvas widget that displays the figure ; it takes the figure instance as a parameter to __init__
        self.canvas_ppg = FigureCanvas(self.figure_ppg)
        self.canvas_accel = FigureCanvas(self.figure_accel)
        # navigation widget ; it takes the canvas widget and a parent
        self.toolbar_ppg = NavigationToolbar(self.canvas_ppg, self)
        self.toolbar_ppg.setMaximumSize(320, 50)
        self.toolbar_accel = NavigationToolbar(self.canvas_accel, self)
        self.toolbar_accel.setMaximumSize(320, 50)
        # ==== layout management ==== #
        # main layout to contain them all
        hbox_main = QtGui.QHBoxLayout()
        # divide main layout into left and right parts
        vbox_right = QtGui.QVBoxLayout()
        widget_pannel = QtGui.QWidget()
        # define horizontal/vertical layout for the left part
        vbox_log = QtGui.QVBoxLayout()
        hbox_log = QtGui.QHBoxLayout()
        # set a fixed width to the left part
        widget_pannel.setLayout(hbox_log)
        widget_pannel.setFixedWidth(200)
        # define a spacer to be used in the left part
        spacer = QtGui.QSpacerItem(0, 50)
        # fill the left part with the widgets
        vbox_log.addWidget(label)
        vbox_log.addItem(spacer)
        vbox_log.addWidget(self.btn_file)
        vbox_log.addWidget(self.file_label)
        vbox_log.addItem(spacer)
        vbox_log.addWidget(self.check_ppg)
        vbox_log.addWidget(self.check_accel)
        vbox_log.addItem(spacer)
        vbox_log.addWidget(self.btn_reset)
        vbox_log.addStretch(1)
        hbox_log.addLayout(vbox_log)
        vbox_log.addStretch(1)
        # define a layout only for the quit button on the bottom right
        # pannel to make it always stay at the bottom right
        vbox_quit = QtGui.QVBoxLayout()
        hbox_quit = QtGui.QHBoxLayout()
        hbox_quit.addStretch(1)
        hbox_quit.addWidget(self.btn_backtomenu)
        hbox_quit.addWidget(self.btn_quit)
        vbox_quit.addLayout(hbox_quit)
        # add left and right pannels to the main layout
        hbox_main.addWidget(widget_pannel)
        hbox_main.addLayout(vbox_right)
        # add plot and quit button to the right part
        vbox_right.addWidget(self.canvas_ppg, 1)  # second argument is a stretch factor
        vbox_right.addWidget(self.toolbar_ppg)
        vbox_right.addWidget(self.canvas_accel, 1)
        vbox_right.addWidget(self.toolbar_accel)
        vbox_right.addLayout(vbox_quit)
        # set all the layout
        self.setLayout(hbox_main)

    def show_file_dialog(self):
        """
        @brief		Display a dialog box to select the .csv file to plot.
        """
        start_browse = sys.path[0] + '/log_sessions/'
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', start_browse, '*.csv')
        # if name is '', it means that no file has been selected
        if fname != '':
            # display name of the selected file
            self.file_label.setText(fname)
            message = 'file ' + fname + ' selected'
            self.parent.printStatus(message)

            # create an axis
            self.ax_ppg = self.figure_ppg.add_subplot(111)
            self.ax_accel = self.figure_accel.add_subplot(111)

            # plot the file selected
            self.plot_file(fname)


        else:
            print('No file selected')
            self.parent.printStatus('No file selected')

    def plot_file(self, csv_file):
        """
        @brief		Plot the .csv file selected.
        @param		csv_file (string): file containing the data to plot.
        """
        timestamp_ppg = []
        ppg = []
        timestamp_accel = []
        accel = []

        tmp = open(csv_file, 'r', newline='')
        tmp_reader = csv.reader(tmp, delimiter=';')

        # skip header
        next(tmp_reader)

        for row in tmp_reader:
            ppg.append(float(row[1]))
            timestamp_ppg.append(datetime.datetime.strptime(str(row[0]), "%Y-%m-%d %H:%M:%S.%f"))
            if row[2] == '[]':
                pass
            else:
                accel.append(float(row[2]))
                timestamp_accel.append(datetime.datetime.strptime(str(row[0]), "%Y-%m-%d %H:%M:%S.%f"))

        # close file when finished
        tmp.close()

        # discards the old graph
        self.ax_ppg.clear()
        self.ax_accel.clear()

        # plot data
        self.ax_ppg.plot(timestamp_ppg, ppg, '-')
        self.ax_accel.plot(timestamp_accel, accel, '-')

        # refresh canvas
        self.canvas_ppg.draw()
        self.canvas_accel.draw()

        self.figure_ppg.tight_layout()
        self.figure_accel.tight_layout()

    def reset_plot(self):
        """
        @brief		Erase the graphs and the file selected.
        """
        self.file_label.setText('')

        # clear the figures
        self.figure_ppg.clear()
        self.figure_accel.clear()
        # refresh canvas
        self.canvas_ppg.draw()
        self.canvas_accel.draw()

        print('Reset done')
        self.parent.printStatus('Reset done')

    def do_check_accel(self):
        """
        @brief		Method linked to the PPG checkbox.
        @details	Hide or show the PPG plot.
        """
        if not self.check_accel.isChecked():
            self.canvas_accel.setHidden(True)
            self.toolbar_accel.setHidden(True)
        if self.check_accel.isChecked():
            self.canvas_accel.setHidden(False)
            self.toolbar_accel.setHidden(False)

    def do_check_ppg(self):
        """
        @brief		Method linked to the PPG checkbox.
        @details	Hide or show the PPG plot.
        """
        if not self.check_ppg.isChecked():
            self.canvas_ppg.setHidden(True)
            self.toolbar_ppg.setHidden(True)
        if self.check_ppg.isChecked():
            self.canvas_ppg.setHidden(False)
            self.toolbar_ppg.setHidden(False)




