import user_interface_toolbox

import sys
import os

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt
import pyqtgraph as pg


class CurrentPlanningUI(user_interface_toolbox.userInterface):
	"""
	@brief		Class to establish the user interface for the StreamingData class.
	"""
	def __init__(self, parent):
		"""
		@brief		Override userInterface __init__ method
		"""
		# use parent __init__
		super(CurrentPlanningUI, self).__init__(parent)
		self.parent = parent
		# instantiate the streaming class
		# and establish the streaming parameters (buffer size, etc.)
		self.stm = streaming_data.StreamingData()
		self.curve_ppg = self.plt.plot(self.stm.x_ppg, self.stm.y_ppg, pen=pg.mkPen('g', width=1))
		self.curve_accel = self.plt2.plot(self.stm.x_accel, self.stm.y_accel, pen=pg.mkPen('r', width=1))

	def createUI(self):
		"""
		@brief		Methods to set up the UI widgets.
		"""
		# ========= icon ========= #
		label = QtGui.QLabel()
		image_path = os.path.join(sys.path[0], './images/logo.png')
		image = QtGui.QPixmap(image_path)
		scaled_image = image.scaled(168, 168, Qt.KeepAspectRatio)
		label.setPixmap(scaled_image)
		# ========= interactive panel elements ========= #
		# define the checkboxes
		self.check_ppg = QtGui.QCheckBox('PPG (green LED)')
		self.check_accel = QtGui.QCheckBox('Accelerometer')
		# default is PPG checked
		self.check_ppg.setChecked(True)
		self.check_accel.setChecked(True)
		# associate an action to the checkboxes
		self.check_ppg.stateChanged.connect(self.do_check_ppg)
		self.check_accel.stateChanged.connect(self.do_check_accel)
		# define the buttons
		self.btn_start = QtGui.QPushButton('start streaming')
		self.btn_pause = QtGui.QPushButton('pause streaming')
		self.btn_stop = QtGui.QPushButton('stop streaming')
		self.btn_quit = QtGui.QPushButton('QUIT')
		# return to main hub menu
		self.btn_backtomenu = QtGui.QPushButton('Back to menu')
		# associate an action to the buttons
		self.btn_start.clicked.connect(self.start)
		self.btn_pause.clicked.connect(self.pause)
		self.btn_stop.clicked.connect(self.stop)
		self.btn_quit.clicked.connect(self.close)
		self.btn_backtomenu.clicked.connect(self.parent.set_hub)
		# serial connection and actions connected to these buttons
		self.find_serial = QtGui.QPushButton('Find serial port(s)')
		self.find_serial.clicked.connect(self.find_port)
		self.list_serial = QtGui.QComboBox()
		self.label_speedport = QtGui.QLabel('Enter baud rate (speed)')
		self.text_speedport = QtGui.QLineEdit('115200')
		# record streaming checkbox
		self.check_record = QtGui.QCheckBox('Record streaming')
		# default is unchecked
		self.check_record.setChecked(False)
		# ========= pyqt graph ========= #
		self.plt = pg.PlotWidget()
		# set up background color
		self.plt.setBackground(background=[60,60,60])
		self.plt.showGrid(x=True, y=True)
		self.plt.setTitle('PPG')
		self.plt.setLabel('left', 'amplitude')
		self.plt.setLabel('bottom', 'time', 's')
		self.plt2 = pg.PlotWidget()
		# set up background color
		self.plt2.setBackground(background=[60,60,60])
		self.plt2.showGrid(x=True, y=True)
		self.plt2.setTitle('Accelerometer')
		self.plt2.setLabel('left', 'amplitude')
		self.plt2.setLabel('bottom', 'time', 's')
		self.max_height = self.parent.height()
		# ========= layout management ========= #
		# main layout containing everything else
		hbox_main = QtGui.QHBoxLayout()
		# divide main layout into left and right parts
		vbox_right = QtGui.QVBoxLayout()
		widget_panel = QtGui.QWidget()
		# define horizontal/vertical layout for the left part
		vbox_stream = QtGui.QVBoxLayout()
		hbox_stream = QtGui.QHBoxLayout()	
		# set a fixed width to the left part
		widget_panel.setLayout(hbox_stream)
		widget_panel.setFixedWidth(185)
		# define a spacer to be used in the left part
		spacer = QtGui.QSpacerItem(0,50)
		# fill the left part with the widgets
		vbox_stream.addWidget(label)
		vbox_stream.addItem(spacer)
		vbox_stream.addWidget(self.find_serial)
		vbox_stream.addWidget(self.list_serial)
		vbox_stream.addWidget(self.label_speedport)
		vbox_stream.addWidget(self.text_speedport)
		vbox_stream.addItem(spacer)
		vbox_stream.addWidget(self.check_ppg)
		vbox_stream.addWidget(self.check_accel)		
		vbox_stream.addItem(spacer)
		vbox_stream.addWidget(self.btn_start)
		vbox_stream.addWidget(self.btn_pause)
		vbox_stream.addWidget(self.btn_stop)
		vbox_stream.addItem(spacer)
		vbox_stream.addWidget(self.check_record)
		vbox_stream.addStretch(1)
		hbox_stream.addLayout(vbox_stream)
		hbox_stream.addStretch(1)
		# define a layout only for the quit button on the bottom right 
		# panel to make it always stay at the bottom right
		vbox_quit = QtGui.QVBoxLayout()
		hbox_quit = QtGui.QHBoxLayout()
		hbox_quit.addStretch(1)
		hbox_quit.addWidget(self.btn_backtomenu)
		hbox_quit.addWidget(self.btn_quit)
		vbox_quit.addLayout(hbox_quit)		
		# add left and right panels to the main layout
		hbox_main.addWidget(widget_panel)
		hbox_main.addLayout(vbox_right)
		# add plot and quit button to the right part
		vbox_right.addWidget(self.plt)
		vbox_right.addWidget(self.plt2)
		vbox_right.addLayout(vbox_quit)
		# set all the layout
		self.setLayout(hbox_main)

	def start(self):
		"""
		@brief		Method linked to the start button.
		@details	Open the serial port selected by the user, set the 
				application timer and call the method from the
				StreamingData class to start the data streaming itself.
		"""		
		# get serial port name and baud rate given by user
		port = str(self.list_serial.currentText())
		speed = int(self.text_speedport.text())
		try:
			# open serial port with name and baud rate given by user
			self.stm.start_streaming(port, speed)
			# define and start the timer to update the plot
			# timer should be as fast as the higher data frequency
			self.timer = QtCore.QTimer()
			self.timer.timeout.connect(self.update_plot)
			# start timer with a timeout interval (in ms)
			self.timer.start(self.stm._interval)
			self.parent.printStatus('Streaming started')
			
			# if streaming started normally:
			# disable the 'Record streaming' checkbox
			self.check_record.setEnabled(False)
			# check the recording checkbox state
			if self.check_record.isChecked():
				self.stm.create_record()
		except serial.SerialException:
			self.parent.printStatus('ERROR - Serial port cannot be opened')
			print('ERROR - Serial port cannot be opened')

	def stop(self):
		"""
		@brief		Method linked to the stop button.
		@details	Stop the application timer, call the method from the StreamingData
				class to stop the streaming itself and close the serial connection.
		"""
		try:
			# stop the timer (and therefore the local loop)
			self.timer.stop()
			self.parent.printStatus('Streaming stopped')
		except:
			# no timer to stop
			pass
		# stop the streaming itself
		try:
			self.stm.stop_streaming()
		except:
			self.parent.printStatus('ERROR while trying to stop streaming')
		
		# reset plot
		self.curve_ppg.clear()
		self.curve_ppg = self.plt.plot(self.stm.x_ppg, self.stm.y_ppg, pen=pg.mkPen('g', width=1))
		self.curve_accel.clear()
		self.curve_accel = self.plt2.plot(self.stm.x_accel, self.stm.y_accel, pen=pg.mkPen('r', width=1))
		
		# close the record if it was open
		if self.check_record.isChecked():
			self.stm.close_record()
		# enable the 'Record streaming' checkbox
		self.check_record.setEnabled(True)

	def pause(self):
		"""
		@brief		Method linked to the pause button.
		@details	Call the method from StreamingData class to pause the streaming.
		"""
		self.stm.pause_streaming()
		self.parent.printStatus('Streaming paused')

	def update_plot(self):
		"""
		@brief		Call the method from StreamingData class to update the data
				and update the curve consequently.
		"""
		# get the new data
		try:
			self.stm.stream_and_log()
		except TypeError:
			pass
		# update the curve 
		self.curve_ppg.setData(self.stm.x_ppg, self.stm.y_ppg)
		self.curve_accel.setData(self.stm.x_accel, self.stm.y_accel)
		# process the events which may be pending if the application is busy performing long operation
		self.parent.app.processEvents()

	def find_port(self):
		"""
		@brief		Method linked to the 'List serial port(s)' button.
		@details	Find the serial port used and list them in the combobox.
		"""	
		# list the items in the QListWidget
		port_items = [self.list_serial.itemText(i) for i in range(self.list_serial.count())]
		
		ports = list(serial.tools.list_ports.comports())
		for port_no, description, address in ports:
			# check if port is not already listed
			if str(port_no)  in port_items:
				pass
			else:
				self.list_serial.addItem(str(port_no))

	def do_check_ppg(self):
		"""
		@brief		Method linked to the PPG checkbox.
		@details	Hide or show the PPG plot.
		"""
		if not self.check_ppg.isChecked():
			self.plt.hide()
			self.plt2.setMaximumHeight(int(self.max_height))
		if self.check_ppg.isChecked():
			self.plt.showNormal()
			if self.check_accel.isChecked():
				self.plt.setMaximumHeight(int(self.max_height/2))
				self.plt2.setMaximumHeight(int(self.max_height/2))
			elif not self.check_accel.isChecked():
				self.plt.setMaximumHeight(int(self.max_height))

	def do_check_accel(self):
		"""
		@brief		Method linked to the accelerometer checkbox.
		@details	Hide or show the accelerometer plot.
		"""
		if not self.check_accel.isChecked():
			self.plt2.hide()
			self.plt.setMaximumHeight(int(self.max_height))	
		if self.check_accel.isChecked():
			self.plt2.showNormal()
			if self.check_ppg.isChecked():
				self.plt.setMaximumHeight(int(self.max_height/2))
				self.plt2.setMaximumHeight(int(self.max_height/2))
			elif not self.check_ppg.isChecked():
				self.plt2.setMaximumHeight(int(self.max_height))

