import user_interface_toolbox

import sys
import os

from PyQt4 import QtGui
from PyQt4.QtCore import Qt
import datetime
sys.path.append('../')


class mainHub(user_interface_toolbox.userInterface):

	def createUI(self):
		"""
		@brief		Set up the user interface.
		"""
		# icon
		label = QtGui.QLabel()
		image_path = os.path.join(sys.path[0], 'images/logo-lcd.jpg')
		image = QtGui.QPixmap(image_path)

		scaled_image = image.scaled(450, 450, Qt.KeepAspectRatio)
		label.setPixmap(scaled_image)

		font = QtGui.QFont()
		font.setPointSize(11)

		self.list_app = QtGui.QComboBox()
		self.list_app.setFont(font)
		self.list_app.addItem('Nouveau Planning')
		self.list_app.addItem('Importer le planning du mois dernier')
		self.list_app.addItem('Importer le planning de ce mois')


		btn_launch = QtGui.QPushButton('OK')
		btn_launch.setFont(font)
		btn_launch.clicked.connect(self.launch_app)

		self.btn_quit = QtGui.QPushButton('Quitter')
		self.btn_quit.clicked.connect(self.close)
		self.btn_quit.setFont(font)

		# layout management
		hub_vbox = QtGui.QVBoxLayout()
		hbox_icon = QtGui.QHBoxLayout()
		hbox_icon.addStretch(1)
		hbox_icon.addWidget(label)
		hbox_icon.addStretch(1)
		hub_hbox = QtGui.QHBoxLayout()
		hub_hbox.addStretch(1)
		hub_hbox.addWidget(self.list_app)
		hub_hbox.addItem(QtGui.QSpacerItem(20,0))
		hub_hbox.addWidget(btn_launch)
		hub_hbox.addStretch(1)

		# define a layout only for the quit button on the bottom right
		# panel to make it always stay at the bottom right
		vbox_quit = QtGui.QVBoxLayout()
		hbox_quit = QtGui.QHBoxLayout()
		hbox_quit.addStretch(1)
		hbox_quit.addWidget(self.btn_quit)
		vbox_quit.addLayout(hbox_quit)

		hub_vbox.addStretch(1)
		hub_vbox.addLayout(hbox_icon)
		hub_vbox.addItem(QtGui.QSpacerItem(0,50))
		hub_vbox.addLayout(hub_hbox)
		hub_vbox.addStretch(1)
		hub_vbox.addLayout(vbox_quit)
		# set all the layout
		self.setLayout(hub_vbox)

	def launch_app(self):
		"""
		@brief	Launch the selected application
		"""
		app_selected = str(self.list_app.currentText())
		if app_selected == 'Nouveau Planning':
			self.parent.set_new_planning()
		if app_selected == 'Importer le planning du mois dernier':
			self.parent.set_last_planning()
		if app_selected == 'Importer le planning de ce mois':
			self.parent.set_current_planning()


